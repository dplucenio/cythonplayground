# -*- coding: utf-8 -*-
"""
drag module definitions
"""
cdef extern from "./drag_laws.hpp":
    double SchillerNaumann1933(double)
    double Dallavalle1948(double)

def SchillerNaumann1933_cpp(Re):
    return SchillerNaumann1933(Re)

def Dallavalle1948_cpp(Re):
    return Dallavalle1948(Re)