/* drag laws */

#include "drag_laws.hpp"
#include <math.h>
#include <algorithm>

double SchillerNaumann1933(double Re)
{
	double C_D = 0.0;

	C_D = std::max(24.0 / Re * (1.0 + 0.15 * pow(Re, 0.687)), 0.44);

	return C_D;
}

double Dallavalle1948(double Re)
{
    double C_D = 0.0;

    C_D = pow(0.63 + 4.8 / sqrt(Re), 2.0);

    return C_D;
}
