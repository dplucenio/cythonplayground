/* drag laws */

#define _USE_MATH_DEFINES 1
#include <cfloat>

double SchillerNaumann1933(double);
double Dallavalle1948(double);
