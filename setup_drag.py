from setuptools import setup, Extension
from Cython.Distutils import build_ext


setup(
    cmdclass={"build_ext": build_ext},
    ext_modules = [
        Extension(
            name="drag",
            sources=["drag.pyx","./drag_laws.cpp"],
            language="c++"
        )
    ]
)
